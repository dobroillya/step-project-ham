export const data = [
    {
        id: 'img1',
        src: './src/images/graphic/graphic-design1.jpg',
        description: 'graphic design 1',
        category: 'graphic'
    },
    {
        id: 'img2',
        src: './src/images/graphic/graphic-design2.jpg',
        description: 'graphic design 2',
        category: 'graphic'
    },
    {
        id: 'img3',
        src: './src/images/graphic/graphic-design3.jpg',
        description: 'graphic design 3',
        category: 'graphic'
    },
    {
        id: 'img4',
        src: './src/images/graphic/graphic-design4.jpg',
        description: 'graphic design 4',
        category: 'graphic'
    },
    {
        id: 'img5',
        src: './src/images/graphic/graphic-design5.jpg',
        description: 'graphic design 5',
        category: 'graphic'
    },
    {
        id: 'img6',
        src: './src/images/graphic/graphic-design6.jpg',
        description: 'graphic design 6',
        category: 'graphic'
    },
    {
        id: 'img7',
        src: './src/images/graphic/graphic-design7.jpg',
        description: 'graphic design 7',
        category: 'graphic'
    },
    {
        id: 'img8',
        src: './src/images/graphic/graphic-design8.jpg',
        description: 'graphic design 8',
        category: 'graphic'
    },
    {
        id: 'img9',
        src: './src/images/graphic/graphic-design9.jpg',
        description: 'graphic design 9',
        category: 'graphic'
    },
    {
        id: 'img10',
        src: './src/images/graphic/graphic-design10.jpg',
        description: 'graphic design 10',
        category: 'graphic'
    },
    {
        id: 'img11',
        src: './src/images/graphic/graphic-design11.jpg',
        description: 'graphic design 11',
        category: 'graphic'
    },
    {
        id: 'img12',
        src: './src/images/graphic/graphic-design12.jpg',
        description: 'graphic design 12',
        category: 'graphic'
    },

    {
        id: 'img13',
        src: './src/images/landing/landing-page1.jpg',
        description: 'landing 1',
        category: 'landing'
    },
    
    {
        id: 'img15',
        src: './src/images/landing/landing-page2.jpg',
        description: 'landing 2',
        category: 'landing'
    },
    {
        id: 'img16',
        src: './src/images/landing/landing-page3.jpg',
        description: 'landing 3',
        category: 'landing'
    },
    {
        id: 'img17',
        src: './src/images/landing/landing-page4.jpg',
        description: 'landing 4',
        category: 'landing'
    },
    {
        id: 'img18',
        src: './src/images/landing/landing-page5.jpg',
        description: 'landing 5',
        category: 'landing'
    },
    {
        id: 'img19',
        src: './src/images/landing/landing-page6.jpg',
        description: 'landing 6',
        category: 'landing'
    },
    {
        id: 'img20',
        src: './src/images/landing/landing-page7.jpg',
        description: 'landing 7',
        category: 'landing'
    },

    {
        id: 'img21',
        src: './src/images/news/news1.jpg',
        description: 'news 1',
        category: 'news'
    },
    {
        id: 'img22',
        src: './src/images/news/news2.jpg',
        description: 'news 2',
        category: 'news'
    },
    {
        id: 'img23',
        src: './src/images/news/news3.jpg',
        description: 'news 3',
        category: 'news'
    },
    {
        id: 'img24',
        src: './src/images/news/news4.jpg',
        description: 'news 4',
        category: 'news'
    },
    {
        id: 'img25',
        src: './src/images/news/news5.jpg',
        description: 'news 5',
        category: 'news'
    },
    {
        id: 'img26',
        src: './src/images/news/news6.jpg',
        description: 'news 6',
        category: 'news'
    },
    {
        id: 'img27',
        src: './src/images/news/news7.jpg',
        description: 'news 7',
        category: 'news'
    },
    {
        id: 'img28',
        src: './src/images/news/news8.jpg',
        description: 'news 8',
        category: 'news'
    },
    {
        id: 'img29',
        src: './src/images/web/web-design1.jpg',
        description: 'web 1',
        category: 'web'
    },

    {
        id: 'img30',
        src: './src/images/web/web-design2.jpg',
        description: 'web 2',
        category: 'web'
    },
    {
        id: 'img31',
        src: './src/images/web/web-design3.jpg',
        description: 'web 3',
        category: 'web'
    },
    {
        id: 'img32',
        src: './src/images/web/web-design4.jpg',
        description: 'web 4',
        category: 'web'
    },
    {
        id: 'img33',
        src: './src/images/web/web-design5.jpg',
        description: 'web 5',
        category: 'web'
    },
    {
        id: 'img34',
        src: './src/images/web/web-design6.jpg',
        description: 'web 6',
        category: 'web'
    },
    {
        id: 'img35',
        src: './src/images/web/web-design7.jpg',
        description: 'web 7',
        category: 'web'
    },
    {
        id: 'img36',
        src: './src/images/web/web-design1.jpg',
        description: 'web 1',
        category: 'web'
    },

    
    {
        id: 'img38',
        src: './src/images/wordpress/wordpress2.jpg',
        description: 'wordpress 2',
        category: 'wordpress'
    },
    {
        id: 'img39',
        src: './src/images/wordpress/wordpress3.jpg',
        description: 'wordpress 3',
        category: 'wordpress'
    },
    {
        id: 'img40',
        src: './src/images/wordpress/wordpress4.jpg',
        description: 'wordpress 4',
        category: 'wordpress'
    },
    {
        id: 'img37',
        src: './src/images/wordpress/wordpress1.jpg',
        description: 'wordpress 1',
        category: 'wordpress'
    },
    {
        id: 'img41',
        src: './src/images/wordpress/wordpress5.jpg',
        description: 'wordpress 5',
        category: 'wordpress'
    },
    {
        id: 'img42',
        src: './src/images/wordpress/wordpress6.jpg',
        description: 'wordpress 6',
        category: 'wordpress'
    },
    {
        id: 'img43',
        src: './src/images/wordpress/wordpress7.jpg',
        description: 'wordpress 7',
        category: 'wordpress'
    },
    {
        id: 'img44',
        src: './src/images/wordpress/wordpress8.jpg',
        description: 'wordpress 8',
        category: 'wordpress'
    },
    {
        id: 'img45',
        src: './src/images/wordpress/wordpress9.jpg',
        description: 'wordpress 9',
        category: 'wordpress'
    },
    {
        id: 'img46',
        src: './src/images/wordpress/wordpress10.jpg',
        description: 'wordpress 10',
        category: 'wordpress'
    },

];
