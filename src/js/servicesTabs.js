function servicesTabs() {
  const tab = document.querySelector(".services-tabs");
  const items = document.querySelectorAll("[data-item]");
  

  tab.addEventListener("click", function (ev){ 

    if (ev.target.dataset.tab) {
      const activeTab = document.querySelector('.services-tabs__item--active');
      activeTab.classList.toggle('services-tabs__item--active');
      ev.target.classList.add('services-tabs__item--active');
      items.forEach((i, index) => {
        if (i.dataset.item === ev.target.dataset.tab) {
          i.classList.remove("services-card--hidden");
        } else {
          i.classList.add("services-card--hidden");
        }        
      });
    }
  });
}

export default servicesTabs;
