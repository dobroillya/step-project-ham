import servicesTabs from "./servicesTabs.js";
import listOfWorks from "./works.js";
import { data } from "./data.js";
import { feedbackData } from "./dataFeedback.js";
import feedBack from "./feedback.js";

window.addEventListener('DOMContentLoaded', () => {    
    servicesTabs();
    listOfWorks(data);
    feedBack(feedbackData);
})