export const feedbackData = [
  {    
    name: "Mariya Hill",
    position: "UX Designer",
    src: "./src/images/faces/face1.jpg",
    said: "Laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. ",
  },
  {
    name: "Elen Martrin",
    position: "CEO",
    src: "./src/images/faces/face2.jpg",
    said: "Integer dignissim, laoreet sem, non Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. ",
  },
  {    
    name: "Hasan Ali",
    position: "Front-end dev",
    src: "./src/images/faces/face3.jpg",
    said: "A fantastic organisation! Great cutomer support from beginning to end of the process. The team are really informed and go the extra mile at every stage. I would recommend them unreservedly Morbi pulvinar odio eget aliquam facilisis.",
  },
  
  {
    
    name: "Alex Li",
    position: "UX Designer",
    src: "./src/images/faces/face4.jpg",
    said: "Great service, efficient communication and a really easy way to get a mortgage with lots of help and support to get the right deal Morbi pulvinar odio eget aliquam facilisis.",
  }
];
