function listOfWorks(data) {
  const ulList = document.querySelector(".work-gallery");
  const ulTabs = document.querySelector(".work-tabs");
  const btnLoad = document.querySelector(".work-gallery__load-more");

  let arrFilter = [...data];
  let startItems = 0;
  let endItems = 12;

  //check if there are items to load
  function checkLoad(arr, end) {
    
    if (arr.length <= end) {
      btnLoad.classList.add("work-gallery--no-load");
    } else {
      btnLoad.classList.remove("work-gallery--no-load");
    }
  }

  function showimages(arr, endNumber, startNumber = 0, addBoolean = true) {
    let htmlInsert = "";
    endNumber > arr.length ? (endNumber = arr.length) : endNumber;

    for (let i = startNumber; i < endNumber; i++) {
      htmlInsert += `
        <li class="work-gallery__item">
          <img src="${arr[i].src}" alt="${arr[i].description}" />
        </li>
        `;
    }
    if (addBoolean) {
      ulList.innerHTML = "";
    }

    ulList.insertAdjacentHTML("beforeend", htmlInsert);
  }

  function randomOrder(arr) {
    arr.sort(() => Math.random() - 0.5);
  }

  randomOrder(arrFilter);
  showimages(arrFilter, endItems, startItems);

  ulTabs.addEventListener("click", (ev) => {
    const btnActive = document.querySelector(".work-tabs__item--active");
    btnActive.classList.remove("work-tabs__item--active");
    ev.target.classList.add("work-tabs__item--active");
    startItems = 0;
    endItems = 12;
    let sortArr = [];
    if (ev.target.dataset.worktab) {
      sortArr = data.filter(
        ({ category }) => category === ev.target.dataset.worktab
      );
    } else {
      sortArr = [...data];
    }
    checkLoad(sortArr, endItems);
    randomOrder(sortArr);
    arrFilter = [...sortArr];
    showimages(sortArr, endItems, startItems);
  });

  btnLoad.addEventListener("click", () => {
    startItems = endItems;
    endItems += 12;

    if (arrFilter.length - endItems > 0) {        
      showimages(arrFilter, endItems, startItems, false);
    } else if (arrFilter.length - startItems > 0) {
      showimages(arrFilter, arrFilter.length, startItems, false);
    }
    checkLoad(arrFilter, endItems);
  });
}

export default listOfWorks;
