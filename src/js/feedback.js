function feedBack(data) {
  let count = 0;
  const arrowRight = document.querySelector(".feedback-menu__arrow--rigth");  
  const addInnerHnml = document.querySelector(".feedback-slider__card");
  const feedbackList = document.querySelector(".feedback-menu__list");

  data.forEach((element, index) => {
    arrowRight.insertAdjacentHTML(
      "beforebegin",
      `<li data-feedback='${index}' class="feedback-menu__item">
          <img
             class="feedback-menu__img"
             src="${element.src}"
             alt="${element.said}"
          />
       </li>`
    );
  });
  const allItem = document.querySelectorAll(".feedback-menu__item");
  allItem[count].classList.add("feedback-menu__item--active");

  function moveItems(increment) {
    const active = document.querySelector(".feedback-menu__item--active");
    active.classList.remove("feedback-menu__item--active");
    
    if (typeof increment === "number") {
      count = increment;
      allItem[count].classList.add("feedback-menu__item--active");
      return;
    }

    if (increment) {
      count++;
      if (count === data.length) {
        count = 0;
      }
    } else {
      count--;
      if (count < 0) {
        count = data.length - 1;
      }
    }
    allItem[count].classList.add("feedback-menu__item--active");
  }

  function showReview({ name, position, src, said }) {
    addInnerHnml.innerHTML = "";
    addInnerHnml.insertAdjacentHTML(
      "afterbegin",
      `
    <div class="feedback-slider__card--active">
        <blockquote class="feedback-slider__quote">      
        ${said}
        <cite class="feedback-slider__author">${name}</cite>
        <p class="feedback-slider__authors-job">${position}</p>
        </blockquote>
        <div class="feedback-slider__img-wrapper">
        <img
            class="feedback-slider__img"
            src="${src}"
            alt="${said}"
        />
        </div>  
    </div> 
  `
    );
  }

  showReview(data[count]);

  feedbackList.addEventListener("click", function (e) {
    const target = e.target.dataset.feedback;
    console.log(target);

    if (target === "rigth") {
      moveItems(true);
      showReview(data[count]);
    }
    if (target === "left") {
      moveItems(false);
      showReview(data[count]);
    }

    if (!Number.isNaN(+target)) {
      console.log(+target);
      moveItems(+target);
      showReview(data[target]);
    }
  });
}

export default feedBack;
